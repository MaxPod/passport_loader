package repo

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"time"

	"github.com/jackc/pgx/v4"
	"gitlab.com/MaxPod/passport_loader/internal/entity"
)

var err error

type repo struct {
	DB     *pgx.Conn
	Config *pgx.ConnConfig
}

// Newrepo - создает объект хранилица
func NewRepo(db *pgx.Conn, cfg *pgx.ConnConfig) (entity.Repo, error) {
	//  Проверим таблицу results_log
	_, err = db.Exec(context.Background(), R1CreateTableResultslog)
	if err != nil {
		return nil, fmt.Errorf("db.Exec: %w, sqlRequest1=%s", err, R1CreateTableResultslog)
	}

	//  Проверим таблицу passports
	_, err = db.Exec(context.Background(), R2CreateTablePassports)
	if err != nil {
		return nil, fmt.Errorf("db.Exec: %w, sqlRequest2=%s", err, R2CreateTablePassports)
	}

	//  Проверим таблицу unformat_passports
	_, err = db.Exec(context.Background(), R3CreateTableUnformatPassports)
	if err != nil {
		return nil, fmt.Errorf("db.Exec: %w, sqlRequest3=%s", err, R3CreateTableUnformatPassports)
	}

	return &repo{
		DB:     db,
		Config: cfg,
	}, nil
}

// GetTotal - возвращает количество паспортов в БД
func (s *repo) GetTotal() (int, error) {
	var total int

	// проверка связи с БД
	s.checkConn()

	err = s.DB.QueryRow(context.Background(), R4SelectTotalFromResultslog).Scan(&total)
	if err != nil {
		return 0, fmt.Errorf("s.DB.QueryRow().Scan(): %w, sqlRequest=%s", err, R4SelectTotalFromResultslog)
	}

	return total, nil
}

// SavePassports - загружает таблицу паспортов из файла
func (s *repo) SavePassports(fileName string) error {
	//  проверим наличие утилиты psql
	err = checkPSQL()
	if err != nil {
		return fmt.Errorf("checkPSQL(): %w", err)
	}

	// проверка связи с БД
	s.checkConn()

	//  готовим таблицу
	_, err = s.DB.Exec(context.Background(), R5CreateTablePassportsNew)
	if err != nil {
		return fmt.Errorf("s.DB.Exec(): %w,  sqlRequest=%s", err, R5CreateTablePassportsNew)
	}

	// загружаем файл в БД
	err = s.copyFileToTable(fileName, "passports_new")
	if err != nil {
		return fmt.Errorf("s.copyFileToTable(): %w", err)
	}

	// добавим индекс
	_, err = s.DB.Exec(context.Background(), R6CreateIndexIDIdxNew)
	if err != nil {
		return fmt.Errorf("s.DB.Exec(): %w,  sqlRequest=%s", err, R6CreateIndexIDIdxNew)
	}

	return nil
}

// SaveUnformatPassports - загружает таблицу неформатных паспортов из файла
func (s *repo) SaveUnformatPassports(fileName string) error {
	//  проверим наличие утилиты psql
	err = checkPSQL()
	if err != nil {
		return fmt.Errorf("checkPSQL(): %w", err)
	}

	// проверка связи с БД
	s.checkConn()

	//  готовим таблицу
	_, err = s.DB.Exec(context.Background(), R7CreateTableUnformatPassportsNew)
	if err != nil {
		return fmt.Errorf("s.DB.Exec(): %w,  sqlRequest=%s", err, R7CreateTableUnformatPassportsNew)
	}

	// загружаем файл в БД
	err = s.copyFileToTable(fileName, "unformat_passports_new")
	if err != nil {
		return fmt.Errorf("s.copyFileToTable(): %w", err)
	}

	return nil
}

// Delivery - обновляет талицы БД для доступа клиента fms
func (s *repo) Delivery(passportsCount, unformatPassportsCount int) error {
	var tx pgx.Tx

	// проверка связи с БД
	s.checkConn()

	totalCount := passportsCount + unformatPassportsCount

	tx, err = s.DB.Begin(context.Background())
	if err != nil {
		return fmt.Errorf("s.DB.Begin(): %w", err)
	}

	defer func() {
		err = tx.Rollback(context.Background())
	}()

	_, err = tx.Exec(context.Background(), R8LockTables)
	if err != nil {
		return fmt.Errorf("tx.Exec(): %w, sqlRequest=%s", err, R8LockTables)
	}

	_, err = tx.Exec(context.Background(), R9ChangeTables)
	if err != nil {
		return fmt.Errorf("tx.Exec(): %w, sqlRequest=%s", err, R9ChangeTables)
	}

	_, err = tx.Exec(context.Background(), R10SaveResults, time.Now(), totalCount, passportsCount, unformatPassportsCount)
	if err != nil {
		return fmt.Errorf("tx.Exec(): %w, sqlRequest=%s", err, R10SaveResults)
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return fmt.Errorf("tx.Commit(): %w", err)
	}

	return nil
}

// copyFileToTable - заливает файл в таблицу через утилиту psql
func (s *repo) copyFileToTable(fileName, tableName string) error {
	connConfig := s.DB.Config()

	runString1 := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?target_session_attrs=read-write",
		connConfig.User,
		connConfig.Password,
		connConfig.Host,
		connConfig.Port,
		connConfig.Database)
	runString2 := "-c"
	runString3 := fmt.Sprintf(`\copy %s FROM '%s' WITH (FORMAT CSV, HEADER TRUE)`, tableName, fileName)

	cmd := exec.Command("psql", runString1, runString2, runString3)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = os.Environ()

	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("cmd.Run(): %w, psql %s%s%s", err, runString1, runString2, runString3)
	}

	return nil
}

// checkPSQL - проверяет наличие утилиты psql
func checkPSQL() error {
	cmd := exec.Command("psql", "-V")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = os.Environ()
	err = cmd.Run()

	if err != nil {
		return fmt.Errorf("cmd.Run(): %w, psql -V", err)
	}

	return nil
}

// setConnActive активирует соединение с БД
func (s *repo) setConnActive() error {
	s.DB.Close(context.Background())

	db, err := pgx.ConnectConfig(context.Background(), s.Config)
	if err != nil {
		return fmt.Errorf("setConnActive(): %w, Database=%s", err, s.Config.Database)
	}

	s.DB = db

	return nil
}

// checkConn проверяет соединение с БД, т.к. оно могло закрыться, пока сервис спал.
// Если соединение закрыто восстанавливает его.
func (s *repo) checkConn() error {
	if s.DB.IsClosed() {
		err := s.setConnActive()

		return err
	}

	return nil
}
