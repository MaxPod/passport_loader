package repo

const (
	R1CreateTableResultslog string = `
		CREATE TABLE IF NOT EXISTS results_log 
		(date timestamp,
		total integer,
		passports_count integer,
		unformat_passports_count integer);`

	R2CreateTablePassports string = `
		CREATE TABLE IF NOT EXISTS passports (id varchar(10));`

	R3CreateTableUnformatPassports string = `
		CREATE TABLE IF NOT EXISTS unformat_passports
		(passp_series varchar(20), 
		 passp_number varchar(20));`

	R4SelectTotalFromResultslog string = `
		SELECT total FROM  results_log WHERE date IN ( SELECT MAX(date) FROM results_log );`

	R5CreateTablePassportsNew string = `
		DROP TABLE IF EXISTS passports_new; 
		CREATE TABLE passports_new (id varchar(10));`

	R6CreateIndexIDIdxNew string = `
		DROP INDEX IF EXISTS id_idx_new; 
		CREATE INDEX id_idx_new ON passports_new (id);`

	R7CreateTableUnformatPassportsNew string = `
		DROP TABLE IF EXISTS unformat_passports_new; 
		CREATE TABLE unformat_passports_new 
		(PASSP_SERIES varchar(20), PASSP_NUMBER varchar(20));`

	R8LockTables string = `
		LOCK TABLE passports IN ACCESS EXCLUSIVE MODE;
		LOCK TABLE unformat_passports IN ACCESS EXCLUSIVE MODE;
		LOCK TABLE passports_new IN ACCESS EXCLUSIVE MODE;
		LOCK TABLE unformat_passports_new IN ACCESS EXCLUSIVE MODE;`

	R9ChangeTables string = `	
		DROP TABLE passports;
		ALTER TABLE passports_new RENAME TO passports;
		DROP INDEX IF EXISTS id_idx;
		ALTER INDEX id_idx_new RENAME TO id_idx;
		DROP TABLE unformat_passports;
		ALTER TABLE unformat_passports_new RENAME TO unformat_passports;`

	R10SaveResults string = `
		INSERT INTO results_log VALUES ($1, $2, $3, $4);`
)
