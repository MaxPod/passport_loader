package source

import (
	"bytes"
	"compress/bzip2"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/MaxPod/passport_loader/internal/entity"
)

var err error

// source - структура источника, реализация интерфейса Source
type source struct {
	archiveFileURL string
}

// NewSource - конструктор обьекта источника
func NewSource(archiveFileURL string) entity.Source {
	return &source{
		archiveFileURL: archiveFileURL,
	}
}

// GetFile - получает файл из инета, распаковывает и удаляет архив
func (s *source) GetFile(workDir string) (string, error) {
	// скачиваем файл от ФМС
	passportArchiveFilePath := filepath.Join(workDir, "list_of_expired_passports.csv.bz2")

	err = downloadFile(s.archiveFileURL, passportArchiveFilePath)
	if err != nil {
		return "", fmt.Errorf("downloadFile(): %w", err)
	}

	// распаковываем архив
	_, err := unarchiveBZ2(passportArchiveFilePath)
	if err != nil {
		return "", fmt.Errorf("unarchiveBZ2(): %w", err)
	}

	// удаляем архив
	os.Remove(passportArchiveFilePath)

	return "list_of_expired_passports.csv", nil
}

// perm -  права на полный доступ к файлу
const perm fs.FileMode = 777

// downloadFile - скачивает файл get  методом из url в указанный путь на диске
func downloadFile(sourceURL, passportArchivePath string) error {
	var buf bytes.Buffer

	resp, err := http.Get(sourceURL)
	if err != nil {
		return fmt.Errorf("http.Get(): %w, sourceURL=%s", err, sourceURL)
	}

	defer resp.Body.Close()

	// nolint:ifshort
	fileSize := resp.ContentLength

	written, err := io.Copy(&buf, resp.Body)
	if err != nil {
		return fmt.Errorf("io.Copy(): %w", err)
	}

	err = ioutil.WriteFile(passportArchivePath, buf.Bytes(), perm)
	if err != nil {
		return fmt.Errorf("ioutil.WriteFile(): %w", err)
	}

	if written != fileSize {
		return errors.New("wrong file length")
	}

	return nil
}

// UnarchiveBZ2 разархивирует архив в текущем каталоге и выдает путь к файлу.
func unarchiveBZ2(filePath string) (string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", fmt.Errorf("os.Open(): %w", err)
	}

	defer f.Close()

	r := bzip2.NewReader(f)

	res := filePath[:len(filePath)-4] // имя файла - это имя архива без .bz2

	out, err := os.Create(res)
	if err != nil {
		return "", fmt.Errorf("os.Create(): %w", err)
	}

	defer out.Close()

	_, err = io.Copy(out, r)
	if err != nil {
		return "", fmt.Errorf("io.Copy(): %w", err)
	}

	return res, nil
}
