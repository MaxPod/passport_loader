package entity

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var err error

// длина строки форматного пасспорта
const FormatPassportLenght int = 10

// Source - интерфейс, источник данных о паспортах
type Source interface {
	GetFile(workDir string) (string, error)
}

// Repo -  интерфейс, хранилище данных
type Repo interface {
	GetTotal() (int, error)
	SavePassports(fileName string) error
	SaveUnformatPassports(fileName string) error
	Delivery(passportsCount, unformatPassportsCount int) error
}

// FmsUpdater - сущность сервиса. Выполняет метод Update() согласно бизнес-логики.
// этапы обработки: download, check-total, vaildate, load-passports, load-unformat-passports, delivery
type FmsUpdater struct {
	workDir                string      //  рабочая директория сервиса для временных файлов
	stage                  string      //  этап обработки
	dataFileName           string      //  имя файла данных
	passportsCount         int         //  число паспортов для загрузки
	unformatPassportsCount int         //  чисто неформатных паспортов для загрузки
	debugMode              bool        //  режим отладки, true = оставляет все файлы в рабочем каталоге
	source                 Source      //  интерфейс работы с источником данных
	repo                   Repo        //  интерфейс хранилища данных
	infoLog                *log.Logger //  пара логеров для инфо сообщений и дебага

}

// NewFmsUpdater - конструктор объекта - сущности сервиса
func NewFmsUpdater(workDir string, debugMode bool, s Source, r Repo) (*FmsUpdater, error) {
	//  проверка существования рабочего каталога
	err = os.MkdirAll(workDir, 0777)
	if err != nil {
		return nil, fmt.Errorf("os.MkdirAll: %w, workDir=%s", err, workDir)
	}

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)

	//  создаем начальную структуру объекта
	f := &FmsUpdater{
		workDir:                workDir,
		stage:                  "download",
		dataFileName:           "",
		passportsCount:         0,
		unformatPassportsCount: 0,
		debugMode:              debugMode,
		source:                 s,
		repo:                   r,
		infoLog:                infoLog,
	}

	//  проверка файла stage
	stageFilePath := filepath.Join(workDir, "stage")
	_, err = os.Stat(stageFilePath)

	//   если файла нет то создадим его
	if os.IsNotExist(err) {
		err = saveToFile(workDir, "stage", f)
		if err != nil {
			return nil, fmt.Errorf("saveToFile: %w", err)
		}
	}

	// все ОК возвращаем объект
	return f, nil
}

// Update - основная функция бизнес-логики, проходит по всем этапам начиная с сохраненного в stage-файле и выполняет их
func (f *FmsUpdater) Update() error {
	f.infoLog.Println("Инициирована процедура обновления данных FMS")

	f.stage, err = f.getStage()
	if err != nil {
		return fmt.Errorf("f.getStage(): %w", err)
	}

	switch f.stage {
	case "download":
		err = f.download()
		if err != nil {
			return fmt.Errorf("f.download(): %w", err)
		}

		fallthrough

	case "check-total":
		var continueUpdate bool

		continueUpdate, err = f.checkTotal()
		if err != nil {
			return fmt.Errorf("f.checkTotal(): %w", err)
		}

		if !continueUpdate {
			break
		}

		fallthrough

	case "vaildate":
		err = f.validate()
		if err != nil {
			return fmt.Errorf("f.vaildate(): %w", err)
		}

		fallthrough

	case "save-passports":
		err = f.savePassports()
		if err != nil {
			return fmt.Errorf("f.savePassports(): %w", err)
		}

		fallthrough

	case "save-unformat-passports":
		err = f.saveUnformatPassports()
		if err != nil {
			return fmt.Errorf("f.saveUnformatPassports(): %w", err)
		}

		fallthrough

	case "delivery":
		err = f.delivery()
		if err != nil {
			return fmt.Errorf("f.delivery(): %w", err)
		}

	default:
		return fmt.Errorf("default: неизвестный этап stage: %s", f.stage)
	}

	return nil
}

// getStage - читает состояние объекта из файла stage
// формат файла stage построчно:
// stage - значение этапа обработки
// имя файла данных
// количество паспортов
// количество неформатных паспортов
func (f *FmsUpdater) getStage() (string, error) {
	var stageFile *os.File

	stageFilePath := filepath.Join(f.workDir, "stage")

	stageFile, err = os.Open(stageFilePath)
	if err != nil {
		return "", fmt.Errorf("os.Open(): %w, stageFilePath=%s", err, stageFilePath)
	}
	defer stageFile.Close()

	scanner := bufio.NewScanner(stageFile)
	scanner.Scan()

	f.stage = scanner.Text()
	scanner.Scan()

	f.dataFileName = scanner.Text()
	scanner.Scan()

	f.passportsCount, err = strconv.Atoi(scanner.Text())
	if err != nil {
		f.passportsCount = 0
	}

	scanner.Scan()

	f.unformatPassportsCount, err = strconv.Atoi(scanner.Text())
	if err != nil {
		f.unformatPassportsCount = 0
	}

	return f.stage, nil
}

// setStage - фиксирует новый этап в объекте и файле
func (f *FmsUpdater) setStage(stage string) error {
	f.stage = stage

	err = saveToFile(f.workDir, "stage", f)
	if err != nil {
		return fmt.Errorf("saveToFile: %w", err)
	}

	return nil
}

// getRowCount - возвращает число строк в файле с учетом заголовка
func (f *FmsUpdater) getRowCount() (int, error) {
	var rowCount int

	var inputFile *os.File

	filePath := filepath.Join(f.workDir, f.dataFileName)

	inputFile, err = os.Open(filePath)
	if err != nil {
		return 0, fmt.Errorf("os.Open(): %w", err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	rowCount = -1 // -1 потому что в первой строчке заголовок

	for scanner.Scan() {
		rowCount++
	}

	return rowCount, nil
}

// handler - обработчик входного файла. проверяет и сортирует данные в два файла
// passports.csv и unformat_passports.csv и посчитывает количество паспортов в каждом файле
func (f *FmsUpdater) handler() error {
	var inputFile, passportsFile, unformatPassportsFile *os.File

	filePath := filepath.Join(f.workDir, f.dataFileName)

	inputFile, err = os.Open(filePath)
	if err != nil {
		return fmt.Errorf("os.Open(): %w", err)
	}
	defer inputFile.Close()

	passportsFilePath := filepath.Join(f.workDir, "passports.csv")

	passportsFile, err = os.Create(passportsFilePath)
	if err != nil {
		return fmt.Errorf("os.Create(passportsFilePath): %w, %s", err, passportsFilePath)
	}
	defer passportsFile.Close()

	unformatPassportsFilePath := filepath.Join(f.workDir, "unformat_passports.csv")

	unformatPassportsFile, err = os.Create(unformatPassportsFilePath)
	if err != nil {
		return fmt.Errorf("os.Create(unformatPassportsFilePath): %w, %s", err, unformatPassportsFilePath)
	}
	defer unformatPassportsFile.Close()

	scanner := bufio.NewScanner(inputFile)

	// обработка первой строки, там  заголовок PASSP_SERIES, PASSP_NUMBER
	// заменим на ID  в passports, в неформатный файл перельем как есть
	scanner.Scan()

	_, err = passportsFile.WriteString("ID\n")
	if err != nil {
		return fmt.Errorf("passportsFile.WriteString(): %w, обработка первой строки", err)
	}

	_, err = unformatPassportsFile.WriteString(scanner.Text() + "\n")
	if err != nil {
		return fmt.Errorf("unformatPassportsFile.WriteString(): %w, обработка первой строки", err)
	}

	passportsCount := 0
	unformatPassportsCount := 0

	for scanner.Scan() {
		//  удалим пробелы
		id := strings.ReplaceAll(scanner.Text(), " ", "")
		// удалим ","
		id = strings.ReplaceAll(id, ",", "")

		// если в строке 10 символов - то считаем паспорт форматным
		if len([]rune(id)) == FormatPassportLenght {
			_, err = passportsFile.WriteString(id + "\n")
			if err != nil {
				return fmt.Errorf("passportsFile.WriteString(): %w, id=%s", err, id)
			}
			passportsCount++
		} else {
			_, err = unformatPassportsFile.WriteString(scanner.Text() + "\n")
			if err != nil {
				return fmt.Errorf("unformatPassportsFile.WriteString(): %w, scanner.Text()=%s", err, scanner.Text())
			}
			unformatPassportsCount++
		}
	}

	if err = scanner.Err(); err != nil {
		return fmt.Errorf("scanner.Err(): %w", err)
	}

	f.passportsCount = passportsCount
	f.unformatPassportsCount = unformatPassportsCount

	return nil
}

// download - этап download, получает файл из источника
func (f *FmsUpdater) download() error {
	f.infoLog.Println("Этап download: скачиваем файл данных")

	f.dataFileName, err = f.source.GetFile(f.workDir)
	if err != nil {
		return fmt.Errorf("f.source.GetFile(): %w", err)
	}

	f.infoLog.Println(" этап завершен: получен файл ", f.dataFileName)

	err = f.setStage("check-total")
	if err != nil {
		return fmt.Errorf("f.setStage(): %w, case=download", err)
	}

	return nil
}

// checkTotal - этап check-total,  проверка количества новых данных
func (f *FmsUpdater) checkTotal() (bool, error) {
	var rowCount, total int

	f.infoLog.Println("Этап check-total: проверяем количество данных")

	rowCount, err = f.getRowCount()
	if err != nil {
		return false, fmt.Errorf("f.getRowCount(): %w", err)
	}

	total, err = f.repo.GetTotal()
	if err != nil {
		f.infoLog.Printf("f.repo.GetTotal(): %v. Установлен total = 0. Работа продолжена.", err)

		total = 0
	}

	different := total - rowCount

	if different >= 0 {
		f.infoLog.Printf(" В файле %d, в хранилище %d. В хранилище больше либо равно на %d. Обновление отменено",
			rowCount,
			total,
			different)

		err = f.setStage("download")
		if err != nil {
			return false, fmt.Errorf("f.setStage(download): %w, case=check-total", err)
		}

		return false, nil
	}

	f.infoLog.Printf(" В файле %d, в хранилище %d. В файле больше на %d. Переход к этапу vaildate",
		rowCount,
		total,
		-different)

	err = f.setStage("vaildate")
	if err != nil {
		return false, fmt.Errorf("f.setStage(vaildate): %w, case=check-total", err)
	}

	return true, nil
}

// validate - этап validate, проверка формата паспортов
func (f *FmsUpdater) validate() error {
	f.infoLog.Println("Этап vaildate: проверяем формат данных")

	err = f.handler()
	if err != nil {
		return fmt.Errorf("f.handler(): %w", err)
	}

	err = f.setStage("save-passports")
	if err != nil {
		return fmt.Errorf("f.setStage(): %w, case=vaildate", err)
	}

	// осатвляем исходный файл если debugMode
	if !f.debugMode {
		filePath := filepath.Join(f.workDir, f.dataFileName)
		os.Remove(filePath)
	}

	f.infoLog.Printf(" этап завершен: отсортировано %d паспортов и %d неформатных паспортов",
		f.passportsCount,
		f.unformatPassportsCount)

	return nil
}

// savePassports - этап savePassports,  сохраняем паспорта в стор
func (f *FmsUpdater) savePassports() error {
	f.infoLog.Println("Этап save-passports: сохраняем паспорта в стор")

	filePath := filepath.Join(f.workDir, "passports.csv")

	err = f.repo.SavePassports(filePath)
	if err != nil {
		return fmt.Errorf("f.repo.SavePassports(): %w", err)
	}

	err = f.setStage("save-unformat-passports")
	if err != nil {
		return fmt.Errorf("f.setStage(): %w, case=save-passports", err)
	}

	//  оставляем исходный файл passports.svc если debugMode
	if !f.debugMode {
		os.Remove(filePath)
	}

	f.infoLog.Println(" паспорта сохранены в стор")

	return nil
}

// saveUnformatPassports - этап saveUnformatPassports, сохраняем неформатные паспорта в стор
func (f *FmsUpdater) saveUnformatPassports() error {
	f.infoLog.Println("Этап save-unformat-passports: сохраняем неформатные паспорта в стор")

	filePath := filepath.Join(f.workDir, "unformat_passports.csv")

	err = f.repo.SaveUnformatPassports(filePath)
	if err != nil {
		return fmt.Errorf("f.repo.SaveUnformatPassports(): %w", err)
	}

	err = f.setStage("delivery")
	if err != nil {
		return fmt.Errorf("f.setStage(): %w, case=save-unformat-passports", err)
	}

	// оставляем исходный файл unformat_passports.svc если debugMode
	if !f.debugMode {
		os.Remove(filePath)
	}

	f.infoLog.Println(" неформатные паспорта сохранены в стор")

	return nil
}

// delivery - этап delivery, обновляем таблицы, записываем итоги
func (f *FmsUpdater) delivery() error {
	f.infoLog.Println("Этап delivery: доставка данных клиенту")

	err = f.repo.Delivery(f.passportsCount, f.unformatPassportsCount)
	if err != nil {
		return fmt.Errorf("f.repo.Delivery(): %w", err)
	}

	err = f.setStage("download")
	if err != nil {
		return fmt.Errorf("f.setStage(): %w, case=delivery", err)
	}

	f.infoLog.Println("Процедура обновления завершена")

	return nil
}

// saveToFile - сохраняет реквизиты объекта в файл
func saveToFile(workDir, fileName string, f *FmsUpdater) error {
	filePath := filepath.Join(workDir, fileName)

	file, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("os.Create(): %w, filePath=%s", err, filePath)
	}

	defer file.Close()

	_, err = file.WriteString(f.stage + "\n")
	if err != nil {
		return fmt.Errorf("file.WriteString(): %w, f.stage=%s", err, f.stage)
	}

	_, err = file.WriteString(f.dataFileName + "\n")
	if err != nil {
		return fmt.Errorf("file.WriteString(): %w, f.dataFileName=%s", err, f.dataFileName)
	}

	_, err = file.WriteString(strconv.Itoa(f.passportsCount) + "\n")
	if err != nil {
		return fmt.Errorf("file.WriteString(): %w, f.passportsCount=%d", err, f.passportsCount)
	}

	_, err = file.WriteString(strconv.Itoa(f.unformatPassportsCount) + "\n")
	if err != nil {
		return fmt.Errorf("file.WriteString(): %w, f.unformatPassportsCount=%d", err, f.unformatPassportsCount)
	}

	return nil
}
