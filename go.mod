module gitlab.com/MaxPod/passport_loader

go 1.16

require (
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jasonlvhit/gocron v0.0.1
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
