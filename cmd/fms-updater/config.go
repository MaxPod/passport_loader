package main

import (
	"gitlab.com/MaxPod/passport_loader/internal/tools/env"
)

// DB - параметры ПГ-хранилица
type DB struct {
	Host          string
	Port          uint16
	User          string
	Password      string
	Database      string
	RuntimeParams map[string]string
}

// Config - конфиг сервиса
type Config struct {
	WorkDir   string
	SourceURL string
	DebugMode bool
	RunTime   string
	DB        DB
}

// Load - загрузчик конфига
func (c *Config) Load() {
	c.WorkDir = env.Get("WORK_DIR").MustString()
	c.SourceURL = env.Get("SOURCE_URL").MustString()
	c.DebugMode = env.Get("DEBUG_MODE").Bool(false)
	c.RunTime = env.Get("RUN_TIME").String("00:00")

	c.DB.Host = env.Get("POSTGRES_HOST").MustString()
	c.DB.Port = uint16(env.Get("POSTGRES_PORT").MustInt())
	c.DB.User = env.Get("POSTGRES_USER").MustString()
	c.DB.Password = env.Get("POSTGRES_PASSWORD").MustString()
	c.DB.Database = env.Get("POSTGRES_DATABASE").MustString()

	c.DB.RuntimeParams = map[string]string{
		"target_session_attrs": "read-write",
	}
}
