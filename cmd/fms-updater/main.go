package main

import (
	"context"
	"log"
	"os"

	"github.com/jackc/pgx/v4"
	"github.com/jasonlvhit/gocron"
	"gitlab.com/MaxPod/passport_loader/internal/entity"
	"gitlab.com/MaxPod/passport_loader/internal/repo"
	"gitlab.com/MaxPod/passport_loader/internal/source"
	"gitlab.com/MaxPod/passport_loader/internal/tools/app"
)

var fmsUpdater *entity.FmsUpdater

var mainLog *log.Logger

var errLog *log.Logger

func main() {
	mainLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime)

	cfg := &Config{}
	cfg.Load()

	mainLog.Println("Init: конфиг загружен успешно")

	s := source.NewSource(cfg.SourceURL)

	mainLog.Println("Init: объект Источник создан")

	dbConfig, _ := pgx.ParseConfig("")
	dbConfig.Host = cfg.DB.Host
	dbConfig.Port = cfg.DB.Port
	dbConfig.User = cfg.DB.User
	dbConfig.Password = cfg.DB.Password
	dbConfig.Database = cfg.DB.Database
	dbConfig.RuntimeParams = cfg.DB.RuntimeParams

	db, err := pgx.ConnectConfig(context.Background(), dbConfig)
	if err != nil {
		errLog.Println("main pgx.ConnectConfig():", err)
		panic(err)
	}

	defer func() {
		db.Close(context.Background())
		mainLog.Println("Final: подключение к БД завершено.", "dbname", cfg.DB.Database)
	}()

	mainLog.Println("Init: подключение к БД активировано.", "dbname", cfg.DB.Database)

	r, err := repo.NewRepo(db, dbConfig)
	if err != nil {
		errLog.Println("main repo.NewRepo():", err)
		panic(err)
	}

	mainLog.Println("Init: объект Репозиторий создан")

	fmsUpdater, err = entity.NewFmsUpdater(cfg.WorkDir, cfg.DebugMode, s, r)
	if err != nil {
		errLog.Println("main entity.NewFmsUpdater():", err)
		panic(err)
	}

	gocron.Every(1).Day().At(cfg.RunTime).Do(FMSUpdateJob)
	mainLog.Println("Init: назначено ежедневное время выполнения задачи Обновление базы FMS", "RunTime", cfg.RunTime)

	app.RunParallel(context.Background(),
		app.SignalNotify,
		gocronStart,
	)
}

// FMSUpdateJob - кронджоба запуска обновления
func FMSUpdateJob() {
	if err := fmsUpdater.Update(); err != nil {
		errLog.Println("main fmsUpdater.Update():", err)
	}
}

// gocronStart - обработка контекста выхода
func gocronStart(ctx context.Context) error {
	stopped := gocron.Start()

	<-ctx.Done()

	gocron.Clear()

	stopped <- true

	return ctx.Err()
}
