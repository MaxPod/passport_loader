FROM golang:1.15-alpine AS builder

ENV CGO_ENABLED 0

ENV TZ=Europe/Moscow

RUN apk --no-cache add ca-certificates tzdata && \
    cp -r -f /usr/share/zoneinfo/$TZ /etc/localtime

WORKDIR /app

COPY . .

RUN go build -mod=vendor -o /fms ./cmd/fms-updater

FROM ubuntu:18.04

RUN apt-get update \
        && apt-get install -y postgresql-client postgresql-client-common

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

COPY --from=builder /etc/localtime /etc/localtime

COPY --from=builder /fms /fms

ENTRYPOINT ["/fms"]